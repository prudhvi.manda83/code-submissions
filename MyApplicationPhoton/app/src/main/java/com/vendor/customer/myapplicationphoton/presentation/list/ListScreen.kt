package com.vendor.customer.myapplicationphoton.presentation.list

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.testTagsAsResourceId
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.vendor.customer.myapplicationphoton.presentation.NavigationScreens
import com.vendor.customer.myapplicationphoton.schoolviewmodel.SchoolViewModel


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun ListViewScreen(name: String, viewModel: SchoolViewModel, navControl:NavHostController, modifier: Modifier = Modifier) {
    if (viewModel.state.schoolsList.size == 0) {
        Box(modifier = Modifier.fillMaxWidth(). fillMaxHeight(), contentAlignment = Alignment.Center) {
            CircularProgressIndicator()
        }
    }
    LazyColumn(modifier = Modifier
        .fillMaxWidth()
        .semantics { testTagsAsResourceId = true }
        .testTag("ALL_SCHOOL_LIST") ) {
        itemsIndexed(viewModel.state.schoolsList, itemContent = { index, item ->
            Column(modifier = Modifier
                .fillMaxWidth()
                .padding(5.dp)
                .clip(shape = RoundedCornerShape(5.dp))
                .background(color = Color.LightGray)
                .padding(5.dp)
                .clickable {
                    println("clicked ${item.schoolName}")
                    viewModel.selectedId(index)
                    navControl.navigate(NavigationScreens.DetailView.id)
                }
                .testTag("ListViewCell${index}"),) {
                Text(text = item.schoolName?:"check")
            }
        })
    }
}
