package com.vendor.customer.myapplicationphoton.presentation

enum class NavigationScreens(val id:String) {
    ListView("listScreen"),
    DetailView("detailsScreen")
}