package com.vendor.customer.myapplicationphoton.schoolviewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vendor.customer.myapplicationphoton.Session
import com.vendor.customer.myapplicationphoton.model.SchoolDetails
import com.vendor.customer.myapplicationphoton.repo.SchoolApi
import kotlinx.coroutines.launch

class SchoolViewModel : ViewModel() {
    var state by mutableStateOf(SchoolScreenState())
    init {
        viewModelScope.launch {
            var api:SchoolApi = Session.getSchoolApi()
            var list:List<SchoolDetails> = api.getSchoolsData()
            println("list ${list.size}")
            state = state.copy(list)
        }
    }

    fun selectedId(index:Int) {
        state = state.copy(selected = index)
    }
}