package com.vendor.customer.myapplicationphoton

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.ViewModelProvider
import com.vendor.customer.myapplicationphoton.presentation.MainScreen
import com.vendor.customer.myapplicationphoton.presentation.list.ListViewScreen
import com.vendor.customer.myapplicationphoton.schoolviewmodel.SchoolViewModel
import com.vendor.customer.myapplicationphoton.ui.theme.MyApplicationPhotonTheme


class MainActivity : ComponentActivity() {
    val schoolViewModel:SchoolViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApplicationPhotonTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MainScreen("Android", schoolViewModel)
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}
/*
@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    MyApplicationPhotonTheme {
        ListViewScreen("Android")
    }
}

 */