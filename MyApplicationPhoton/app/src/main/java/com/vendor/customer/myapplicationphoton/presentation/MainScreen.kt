package com.vendor.customer.myapplicationphoton.presentation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.vendor.customer.myapplicationphoton.presentation.details.DetailsViewScreen
import com.vendor.customer.myapplicationphoton.presentation.list.ListViewScreen
import com.vendor.customer.myapplicationphoton.schoolviewmodel.SchoolViewModel




@Composable
fun MainScreen(name: String, viewModel: SchoolViewModel, modifier: Modifier = Modifier, navControl:NavHostController = rememberNavController()) {
    NavHost(navController = navControl, startDestination = NavigationScreens.ListView.id) {
        composable(route = NavigationScreens.ListView.id) {
            ListViewScreen(name = "ListView", viewModel = viewModel, navControl)
        }
        composable(route = NavigationScreens.DetailView.id) {
            DetailsViewScreen(name = "DetailsView", viewModel = viewModel)
        }
    }
}
