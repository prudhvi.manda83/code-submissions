package com.vendor.customer.myapplicationphoton.repo
import com.vendor.customer.myapplicationphoton.model.SchoolDetails
import retrofit2.http.GET

interface SchoolApi {

    @GET("s3k6-pzi2.json")
    suspend fun getSchoolsData():List<SchoolDetails>

}