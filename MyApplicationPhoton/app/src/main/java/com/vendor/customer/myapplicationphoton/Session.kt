package com.vendor.customer.myapplicationphoton

import com.vendor.customer.myapplicationphoton.repo.SchoolApi
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Session {

    val BASE_URL = "https://data.cityofnewyork.us/resource/"
    val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    fun getSchoolApi():SchoolApi {
        return retrofit.create(SchoolApi::class.java)
    }
}