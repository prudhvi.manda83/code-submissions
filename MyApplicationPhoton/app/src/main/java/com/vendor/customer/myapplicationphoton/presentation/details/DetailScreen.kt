package com.vendor.customer.myapplicationphoton.presentation.details

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.vendor.customer.myapplicationphoton.schoolviewmodel.SchoolViewModel

@Composable
fun DetailsViewScreen(name: String, viewModel: SchoolViewModel, modifier: Modifier = Modifier) {
    if (viewModel.state.selected >= 0) {
        viewModel.state.schoolsList[viewModel.state.selected].overviewParagraph?.let {
            Text(
                text = it ?: ""
            )
        }
    } else {
        Text("School not selected")
    }
}