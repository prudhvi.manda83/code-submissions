package com.vendor.customer.myapplicationphoton.schoolviewmodel

import com.vendor.customer.myapplicationphoton.model.SchoolDetails

data class SchoolScreenState(
    val schoolsList:List<SchoolDetails> = emptyList(),
    val selected:Int = -1
)
