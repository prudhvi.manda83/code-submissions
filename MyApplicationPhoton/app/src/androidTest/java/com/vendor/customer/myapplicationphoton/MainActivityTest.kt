package com.vendor.customer.myapplicationphoton

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onAllNodesWithTag
import androidx.compose.ui.test.onChildren
import androidx.compose.ui.test.onFirst
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import com.vendor.customer.myapplicationphoton.presentation.MainScreen
import com.vendor.customer.myapplicationphoton.presentation.list.ListViewScreen
import com.vendor.customer.myapplicationphoton.schoolviewmodel.SchoolViewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MainActivityTest {

    @get: Rule
    val composeTestRule = createComposeRule()

    val viewModel = SchoolViewModel()
    @Test
    fun checkUI(){
        composeTestRule.setContent {
            MainScreen(name = "test", viewModel = viewModel)
        }
        composeTestRule
            .waitUntil(
                timeoutMillis = 5000
            ) {
                composeTestRule.onNodeWithTag("ALL_SCHOOL_LIST").fetchSemanticsNode().isRoot==false
            }

    }

}